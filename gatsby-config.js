module.exports = {
  siteMetadata: {
    title: "MATESZE",
    author: "MATESZE",
    description: "Magyar Terápiás és Segítőkutyás Szövetség Egyesület",
    siteUrl: "https://gatsby-starter-blog-demo.netlify.com/",
    social: {
      twitter: "matesze_hu",
    },
  },
  plugins: [
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: `${__dirname}/content/blog`,
        name: "blog",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: `${__dirname}/content/assets`,
        name: "assets",
      },
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: "gatsby-remark-responsive-iframe",
            options: {
              wrapperStyle: "margin-bottom: 1.0725rem",
            },
          },
          "gatsby-remark-prismjs",
          "gatsby-remark-copy-linked-files",
          "gatsby-remark-smartypants",
        ],
      },
    },
    {
      resolve: "gatsby-transformer-sharp",
      options: {},
    },
    {
      resolve: "gatsby-plugin-sharp",
      options: {},
    },
    {
      resolve: "gatsby-plugin-google-analytics",
      options: {
        trackingId: "213219885", // matesze-hu
      },
    },
    {
      resolve: "gatsby-plugin-feed",
      options: {},
    },
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "MATESZE",
        short_name: "MATESZE",
        start_url: "/",
        background_color: "#ffffff",
        theme_color: "#663399",
        display: "minimal-ui",
        icon: "src/assets/mateszeLogo.svg",
      },
    },
    {
      resolve: "gatsby-plugin-offline",
      options: {},
    },
    {
      resolve: "gatsby-plugin-react-helmet",
      options: {},
    },
    {
      resolve: "gatsby-plugin-typography",
      options: {
        pathToConfigModule: "src/utils/typography",
      },
    },
    {
      resolve: "gatsby-plugin-typescript",
      options: {},
    },
    {
      resolve: "gatsby-plugin-material-ui",
      options: {},
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /assets/,
        },
      },
    },
    // { // once V2 is in place
    //   resolve: "gatsby-plugin-transition-link",
    //   options: {},
    // },
    {
      resolve: `gatsby-plugin-intl`,
      options: {
        path: `${__dirname}/src/intl`,
        languages: ["hu"], //, "de", "en"],
        defaultLanguage: "hu",
        redirect: false,
      },
    },
  ],
};
