import Typography from "typography";

const typography = new Typography({
  googleFonts: [
    {
      name: "Work Sans",
      styles: ["300", "400", "700"],
    },
  ],
  baseLineHeight: 1.45,
  headerFontFamily: ["Work Sans", "Arial"],
  bodyFontFamily: ["Work Sans", "Arial"],
  headerWeight: 700,
  overrideStyles: ({ adjustFontSizeTo, rhythm }, options, styles) => ({
    html: {
      overflowY: "auto",
    },
  }),
});

// Hot reload typography in development.
if (process.env.NODE_ENV !== `production`) {
  typography.injectStyles();
}

export default typography;
export const rhythm = typography.rhythm;
export const scale = typography.scale;
