import { createMuiTheme, Theme } from "@material-ui/core/styles";

const theme1: Theme = createMuiTheme({
  typography: {
    fontFamily: [
      "Roboto",
      "Helvetica Neue",
      "Arial",
      "sans-serif",
    ].join(","),
  },
});

const theme2: Theme = createMuiTheme({
  typography: {
    htmlFontSize: 14,
    fontFamily: [
      "Work Sans",
      "Roboto",
      "Helvetica Neue",
      "Arial",
      "sans-serif",
    ].join(","),
  },
});

export const theme: Theme = theme1;
export const indexTheme : Theme  = theme2;
