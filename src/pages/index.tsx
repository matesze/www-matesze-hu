import React, { Fragment } from "react";
import { graphql, Link } from "gatsby";
import BackgroundImage from "gatsby-background-image";

import {
  createStyles,
  makeStyles,
  Theme,
  ThemeProvider,
} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {
  Box,
  Button,
  CssBaseline,
  Divider,
  Paper,
  Typography,
} from "@material-ui/core";
// @ts-ignore
import MateszeLogo from "../assets/mateszeLogo.svg";
import SEO from "../components/seo";
import { indexTheme } from "../utils/theme";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    html: {
      fontSize: "18px",
    },
    root: {
      flexGrow: 1,
    },
    box: {
      padding: theme.spacing(2),
    },
    button: {
      margin: theme.spacing(1),
    },
    paper: {
      background: "transparent",
      padding: theme.spacing(6),
      paddingTop: theme.spacing(12),
      verticalAlign: "middle",
      overflow: "auto",
      height: "100vh",
    },
  })
);

interface WelcomePageProps {
  location: Location;
  data?: any;
}

export default function WelcomePage({ location, data }: WelcomePageProps) {
  const classes = useStyles();

  return (
    <Fragment>
      <ThemeProvider theme={indexTheme}>
        <CssBaseline />
        <SEO title="Home" lang="hu" />
        <div className={classes.root}>
          <Grid
            item
            xs={12}
            sm={12}
            style={{
              position: "fixed",
              zIndex: 100,
              width: "100%",
            }}
          >
            <Paper
              elevation={0}
              style={{ backgroundColor: "transparent", padding: "10px" }}
            >
              <Grid
                container
                spacing={1}
                direction="row"
                justify="center"
                alignItems="center"
              >
                <Grid item>
                  <Button
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    href="#home"
                  >
                    MATESZE
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    href="#hireink"
                  >
                    Híreink
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    href="#magunkrol"
                  >
                    Magunkról
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    href="#kepzesek"
                  >
                    Képzések
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    href="#adatbazis"
                  >
                    Adatbázis
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    href="#szervezetek"
                  >
                    Szervezetek
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    href="#dokumentumok"
                  >
                    Dokumentumok
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </Grid>

          <Grid container spacing={0}>
            <Grid item xs={12} sm={6} id="home">
              <Paper elevation={0} className={classes.paper}>
                <Box className={classes.box} width={"150px"}>
                  <MateszeLogo />
                </Box>
                <Typography variant="h4" gutterBottom>
                  Üdvözöljük a
                </Typography>
                <Typography variant="h3" gutterBottom>
                  Magyar Terápiás és Segítőkutyás Szövetség Egyesület
                </Typography>
                <Typography variant="h4" gutterBottom>
                  honlapján
                </Typography>
                <Typography variant="body1" gutterBottom>
                  Keressen a szervezetek, kiképzők és kiképzett kutyák
                  adatbázisában. Ismerje meg a MATESZE-t és tagszervezeteit.
                </Typography>
              </Paper>
            </Grid>

            <Grid item xs={12} sm={6}>
              <BackgroundImage
                fluid={data.doggie1.childImageSharp.fluid}
                alt={"doggie1"}
              >
                <Paper elevation={0} className={classes.paper}></Paper>
              </BackgroundImage>
            </Grid>

            <Grid item xs={12} sm={6}>
              <BackgroundImage
                fluid={data.doggie6.childImageSharp.fluid}
                alt={"doggie6"}
              >
                <Paper elevation={0} className={classes.paper}></Paper>
              </BackgroundImage>
            </Grid>

            <Grid item xs={12} sm={6} id="hireink">
              <Paper elevation={0} className={classes.paper}>
                <Typography variant="h2" gutterBottom>
                  Híreink
                </Typography>

                <Typography variant="h4" gutterBottom>
                  Az alapvető jogok biztosának jelentése
                </Typography>
                <Typography variant="body1" gutterBottom>
                  Az alapvető jogok biztosának jelentése a terápiás kutyák
                  egészségügyi intézményben való alkalmazásáról.
                  <br />
                  <Button className={classes.button} variant="outlined">
                    Tovább
                  </Button>
                </Typography>
                <Divider />
                <Typography variant="h4" gutterBottom>
                  MATESZE közgyűlés
                </Typography>
                <Typography variant="body1" gutterBottom>
                  MATESZE közgyűlési meghívó.
                  <br />
                  <Button className={classes.button} variant="outlined">
                    Tovább
                  </Button>
                </Typography>
                <Divider />
                <Typography variant="h4" gutterBottom>
                  Konferencia
                </Typography>
                <Typography variant="body1" gutterBottom>
                  A Pata és Mancs Terápiás és Oktatási Egyesület 2019.május
                  18-án konferenciát szervez.
                  <br />
                  <Button className={classes.button} variant="outlined">
                    Tovább
                  </Button>
                </Typography>
                <Divider />
              </Paper>
            </Grid>

            <Grid item xs={12} sm={6} id="magunkrol">
              <Paper elevation={0} className={classes.paper}>
                <Typography variant="h2" gutterBottom>
                  Magunkról
                </Typography>
                <Typography variant="body1" gutterBottom>
                  A honlap azért jött létre, mert a társadalmi esélyegyenlőség
                  előmozdításáért felelős miniszter a MATESZ Egyesületet jelölte
                  ki közreműködő szervezetként, hogy a 27/2009. (12.03.) számú
                  SZMM rendeletben meghatározott feladatokat ellássa. A Magyar
                  Terápiás és Segítőkutyás Szövetség (MATESZE) 2006-ban azzal a
                  céllal alakult, hogy érdekvédelmi és szakmai
                  „ernyőszervezetként” összefogja azokat a civil
                  kezdeményezéseket, melyek segítő kutyák képzésével és
                  rászorultakhoz való eljuttatásával foglalkoznak. Elsődleges
                  célunk, hogy a szakmai standardok kidolgozásával és
                  érvényesítésével olyan egységes szakmai hátteret alakítsunk
                  ki, amely biztosítéka lehet annak, hogy a társadalom elfogadja
                  a kutyát, mint „rehabilitációs eszközt”, hogy azok valóban jó
                  hatásfokkal és széles körű elfogadásra találva fejthessék ki a
                  fogyatékos-rehabilitáció területén munkájukat.
                </Typography>
              </Paper>
            </Grid>

            <Grid item xs={12} sm={6}>
              <BackgroundImage
                fluid={data.doggie4.childImageSharp.fluid}
                alt={"doggie4"}
              >
                <Paper elevation={0} className={classes.paper}></Paper>
              </BackgroundImage>
            </Grid>

            <Grid item xs={12} sm={6}>
              <BackgroundImage
                fluid={data.doggie5.childImageSharp.fluid}
                alt={"doggie5"}
              >
                <Paper elevation={0} className={classes.paper}></Paper>
              </BackgroundImage>
            </Grid>

            <Grid item xs={12} sm={6} id="kepzesek">
              <Paper elevation={0} className={classes.paper}>
                <Typography variant="h2" gutterBottom>
                  Képzések
                </Typography>
                <Typography variant="body1" gutterBottom>
                  Keressen a szervezetek, kiképzők és <br />
                  kiképzett kutyák adatbázisában. <br />
                  Ismerje meg a MATESZE-t, tagszervezeteit.
                </Typography>
              </Paper>
            </Grid>

            <Grid item xs={12} sm={6} id="adatbazis">
              <Paper elevation={0} className={classes.paper}>
                <Typography variant="h2" gutterBottom>
                  Adatbázis
                </Typography>
                <Typography variant="body1" gutterBottom>
                  Keressen a szervezetek, kiképzők és <br />
                  kiképzett kutyák adatbázisában. <br />
                  Ismerje meg a MATESZE-t, tagszervezeteit.
                </Typography>
                <Button
                  className={classes.button}
                  variant="outlined"
                  href="/adatbazis"
                >
                  Belépés
                </Button>
              </Paper>
            </Grid>

            <Grid item xs={12} sm={6}>
              <BackgroundImage
                fluid={data.doggie7.childImageSharp.fluid}
                alt={"doggie7"}
              >
                <Paper elevation={0} className={classes.paper}></Paper>
              </BackgroundImage>
            </Grid>

            <Grid item xs={12} sm={6}>
              <BackgroundImage
                fluid={data.doggie8.childImageSharp.fluid}
                alt={"doggie8"}
              >
                <Paper elevation={0} className={classes.paper}></Paper>
              </BackgroundImage>
            </Grid>

            <Grid item xs={12} sm={6} id="szervezetek">
              <Paper elevation={0} className={classes.paper}>
                <Typography variant="h2" gutterBottom>
                  Szervezetek
                </Typography>
                <Typography variant="body1" gutterBottom>
                  A honlapon csak habilitációskutya-kiképző szervezetek
                  regisztrálhatnak. A 27/2009. SZMM rendelet értelmében az a
                  szervezet minősül segítőkutya kiképző szervezetnek, amelynek
                  <ol>
                    <li>
                      létesítő okiratában a segítő kutyák kiképzése és a
                      fogyatékossággal élő személyekhez történő eljuttatása
                      feltüntetésre került, és
                    </li>
                    <li>
                      amely biztosítja, hogy a kiképzést olyan kiképző végzi,
                      aki az Országos Képzési Jegyzékben szereplő habilitációs
                      kutyakiképző szakképesítéssel rendelkezik.
                    </li>
                  </ol>
                </Typography>
                <Typography variant="body1" gutterBottom>
                  Ha már regisztrált:
                </Typography>
                <Button
                  className={classes.button}
                  variant="outlined"
                  href="/adatbazis"
                >
                  Belépés
                </Button>
                <Typography variant="body1" gutterBottom>
                  Ha regisztrálni kíván:
                </Typography>
                <Button
                  className={classes.button}
                  variant="outlined"
                  href="/adatbazis"
                >
                  Regisztráció
                </Button>
              </Paper>
            </Grid>

            <Grid item xs={12} sm={6} id="dokumentumok">
              <Paper elevation={0} className={classes.paper}>
                <Typography variant="h2" gutterBottom>
                  Dokumentumok
                </Typography>
                <Typography variant="body1" gutterBottom>
                  Hasznos, letölthető dokumentumok és jelentések.
                </Typography>
                <Button
                  className={classes.button}
                  variant="outlined"
                  href="/adatbazis"
                >
                  Dokumentumtár
                </Button>
              </Paper>
            </Grid>

            <Grid item xs={12} sm={6}>
              <BackgroundImage
                fluid={data.doggie9.childImageSharp.fluid}
                alt={"doggie9"}
              >
                <Paper elevation={0} className={classes.paper}></Paper>
              </BackgroundImage>
            </Grid>

            <Grid container spacing={0}>
              <Grid item xs={12}>
                <Paper elevation={0} className={classes.paper}>
                  <Grid container>
                    <Grid item xs={4}>
                      <Box className={classes.box} width={"120px"}>
                        <MateszeLogo />
                      </Box>
                      <Typography variant="h6" gutterBottom>
                        Magyar Terápiás és Segítőkutyás <br /> Szövetség
                        Egyesület
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        4031 Debrecen, Béla utca 2. <br />
                        info@matesze.hu
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Typography variant="h6" gutterBottom>
                        Oldalak
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Híreink</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Magunkról</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Képzések</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Adatbázis</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Szervezetek</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Dokumentumok</Link>
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Typography variant="h6" gutterBottom>
                        Nyilatkozatok
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Adatvédelem</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Adatkezelés</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">GDPR</Link>
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Typography variant="h6" gutterBottom>
                        Nyilatkozatok
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Adatvédelem</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Adatkezelés</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">GDPR</Link>
                      </Typography>
                    </Grid>
                    <Grid item xs={2} alignItems="center">
                      <Typography variant="h6" gutterBottom>
                        Jelentések
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Közhasznúsági jelentések</Link>
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        <Link to="#">Statisztikák</Link>
                      </Typography>
                    </Grid>

                    <Grid item xs={12}>
                      <Typography variant="body2" align="right">
                        MATESZE, minden jog fenntartva. 2019.
                        <br />
                        Fényképek: <a href="https://unsplash.com/">Unsplash</a>.
                      </Typography>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </ThemeProvider>
    </Fragment>
  );
}

export const query = graphql`
  query {
    doggie1: file(relativePath: { in: "doggie-1.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie2: file(relativePath: { in: "doggie-2.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie3: file(relativePath: { in: "doggie-3.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie4: file(relativePath: { in: "doggie-4.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie5: file(relativePath: { in: "doggie-5.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie6: file(relativePath: { in: "doggie-6.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie7: file(relativePath: { in: "doggie-7.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie8: file(relativePath: { in: "doggie-8.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    doggie9: file(relativePath: { in: "doggie-9.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;
